// This class is not distributed to students

package slp;

import java.util.zip.CRC32;
import java.util.zip.Checksum;

import core.Msg;
import exceptions.BadChecksumException;
import exceptions.IWProtocolException;
import exceptions.IllegalAddrException;
import exceptions.IllegalMsgException;

/* 
 * Simple link protocol data message fields:
 *  -> DataHeader
 * 	-> dst addr
 *  -> src addr
 *  -> data length
 *  -> data
 *  -> checksum
*/

public class SLPDataMsg extends SLPMsg {


}
